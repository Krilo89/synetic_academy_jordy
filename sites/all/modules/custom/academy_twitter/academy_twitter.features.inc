<?php
/**
 * @file
 * academy_twitter.features.inc
 */

/**
 * Implements hook_views_api().
 */
function academy_twitter_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
