<?php
/**
 * @file
 * academy_medewerker.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function academy_medewerker_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access user profiles'.
  $permissions['access user profiles'] = array(
    'name' => 'access user profiles',
    'roles' => array(
      'Beheerder' => 'Beheerder',
      'administrator' => 'administrator',
    ),
    'module' => 'user',
  );

  // Exported permission: 'administer profile types'.
  $permissions['administer profile types'] = array(
    'name' => 'administer profile types',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'profile2',
  );

  // Exported permission: 'administer profile2_regpath'.
  $permissions['administer profile2_regpath'] = array(
    'name' => 'administer profile2_regpath',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'profile2_regpath',
  );

  // Exported permission: 'administer profiles'.
  $permissions['administer profiles'] = array(
    'name' => 'administer profiles',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'profile2',
  );

  // Exported permission: 'edit any medewerker profile'.
  $permissions['edit any medewerker profile'] = array(
    'name' => 'edit any medewerker profile',
    'roles' => array(
      'Beheerder' => 'Beheerder',
    ),
    'module' => 'profile2',
  );

  // Exported permission: 'edit own medewerker profile'.
  $permissions['edit own medewerker profile'] = array(
    'name' => 'edit own medewerker profile',
    'roles' => array(
      'Beheerder' => 'Beheerder',
    ),
    'module' => 'profile2',
  );

  // Exported permission: 'view any medewerker profile'.
  $permissions['view any medewerker profile'] = array(
    'name' => 'view any medewerker profile',
    'roles' => array(
      'Beheerder' => 'Beheerder',
    ),
    'module' => 'profile2',
  );

  // Exported permission: 'view own medewerker profile'.
  $permissions['view own medewerker profile'] = array(
    'name' => 'view own medewerker profile',
    'roles' => array(
      'Beheerder' => 'Beheerder',
      'Medewerker' => 'Medewerker',
    ),
    'module' => 'profile2',
  );

  return $permissions;
}
