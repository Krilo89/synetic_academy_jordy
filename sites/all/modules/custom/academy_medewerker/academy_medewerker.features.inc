<?php
/**
 * @file
 * academy_medewerker.features.inc
 */

/**
 * Implements hook_default_profile2_type().
 */
function academy_medewerker_default_profile2_type() {
  $items = array();
  $items['medewerker'] = entity_import('profile2_type', '{
    "userCategory" : true,
    "userView" : true,
    "type" : "medewerker",
    "label" : "Medewerker",
    "weight" : "0",
    "data" : { "registration" : 1 },
    "rdf_mapping" : []
  }');
  return $items;
}
