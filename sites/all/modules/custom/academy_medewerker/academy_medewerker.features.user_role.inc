<?php
/**
 * @file
 * academy_medewerker.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function academy_medewerker_user_default_roles() {
  $roles = array();

  // Exported role: Medewerker.
  $roles['Medewerker'] = array(
    'name' => 'Medewerker',
    'weight' => 4,
  );

  return $roles;
}
