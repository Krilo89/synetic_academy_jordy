<?php

/**
 * @file
 * AcademyWorklogMetadataController extends EntityDefaultMetadataController.
 *
 * Extending the EntityDefaultMetadataController for the AcademyWorklog entity.
 * use this instead of hook_entity_info_alter().
 */

/**
 * {@inheritdoc}
 */
class AcademyWorklogMetadataController extends EntityDefaultMetadataController {

  /**
   * {@inheritdoc}
   */
  public function entityPropertyInfo() {

    $info = parent::entityPropertyInfo();

    $properties = &$info[$this->type]['properties'];
    $properties['customer_id'] = array(
      'label' => t("Customer"),
      'type' => 'node',
      'description' => t("The Customer node"),
      'setter permission' => 'create worklog entities',
      'required' => TRUE,
      'schema field' => 'customer_id',
    );
    $properties['employee_id'] = array(
      'label' => t("Employee"),
      'type' => 'user',
      'description' => t("The Employee user"),
      'setter permission' => 'create worklog entities',
      'required' => TRUE,
      'schema field' => 'employee_id',
    );

    return $info;
  }

}
