<?php

/**
 * @file
 * Eigen bedankt pagina voor een webform formulier.
 *
 * Available variables:
 * - $userData:
 *  - userName,
 *  - userEmail,
 *  - urenMax,
 *  - urenWorked.
 * - $week_number
 */
?>

<style type="text/css">
  /* Styles for worklog--weekly-update.tpl.php */
  body {
    font-family: "Arial", 'sans-serif';
    font-size: 14px;
  }
  .worklog--weekly-update {
    position: relative;
    width: 100%;
    padding-top: 30px;
    padding-bottom: 30px;
    background: #fff;
  }
  .worklog--weekly-update__content {
    margin: 0 auto;
    padding: 30px;
    border: 1px solid #ddd;
    width: 660px;
  }
  .worklog--weekly-update table {
    width: 100%;
    border-spacing: 0;
    border-collapse: collapse;
    font-size: 14px;
  }
  .worklog--weekly-update th,
  .worklog--weekly-update td {
    border: 1px solid #ddd;
    text-align: left;
    padding: 4px 8px;
  }
  .worklog--weekly-update th {
    padding-top: 8px;
    padding-bottom: 8px;
    font-size: 16px;
    color: #fff;
    background-color: #bbb;
  }
</style>

<div class="worklog--weekly-update">

  <div class="worklog--weekly-update__content">
    <h3><?php print t('Dear @manager_username,', array('@manager_username' => $manager_username)); ?></h3><br />
    <p><?php print t('Below are the weekly results about the worked hours per Employee for week: @week_number.', array('@week_number' => $week_number)); ?></p><br />

    <?php print $worklog_results_weekly_overview; ?>
  </div>

</div>
