<?php
/**
 * @file
 * academy_worklog.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function academy_worklog_user_default_roles() {
  $roles = array();

  // Exported role: Beheerder.
  $roles['Beheerder'] = array(
    'name' => 'Beheerder',
    'weight' => 3,
  );

  return $roles;
}
