<?php
/**
 * @file
 * Code for the Worklog settings page.
 *
 * A user can fill in the UID of the person who is the Manager and receives
 * an weekly email update about the worked hours.
 */

/**
 * Webform admin settings page callback, referenced in hook_menu().
 */
function academy_worklog_settings($form, &$form_state) {

  $form = array();

  $form['academy_worklog_manager_user_uid'] = array(
    '#type' => 'textfield',
    '#title' => t('Worklog Manager UID'),
    '#description' => t('Enter the UID of the Manager who must receive the
      weekly update about the logged hours per Employee.'),
    '#default_value' => variable_get('academy_worklog_manager_user_uid'),
  );

  return system_settings_form($form);

}