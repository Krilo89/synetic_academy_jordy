/**
 *
 * @file
 */
(function ($) {
  /**
   * Give feedback to the user when Hours are entered. Based on the Hours that are worked for that week.
   *
   * @type
   */
  Drupal.behaviors.academyWorklog = {
    attach: function (context, settings) {
      // Attach listener to the Hour field and initialize.
      var $hourField = $(".form-item-hours input").once('academyWorklog');
      if ($hourField.length > 0) {
        // Make a div behind the Hour Field, where we can put our message in.
        $hourField.after($('<div>', {
            "class": 'hours__message'
          }
        ));

        $hourField.bind("change paste input",
          Drupal.behaviors.academyWorklog.setHourFieldStatus
        );
        Drupal.behaviors.academyWorklog.setHourFieldStatus.call($hourField);
      }
    },
    /**
     * Update Hour field based in the Hours filled in. Bad is red, good is green.
     */
    setHourFieldStatus: function () {
      var $hourField = $(this);
      var $workedHours = $hourField.val();

      var hoursRemainingThisWeek = Drupal.settings.academy_worklog.hours_remaining_this_week - $workedHours;

      // When you have more hours left than the half of your Maximum, give a red border, otherwise green.
      if (hoursRemainingThisWeek > (Drupal.settings.academy_worklog.max_hours_per_week / 2)) {
        $hourField.addClass('warning').removeClass('success');
      }
      else {
        $hourField.addClass('success').removeClass('warning');
      }

      // Put text message after Hour_Field.
      var currentHoursTextMessage = Drupal.t('You have @hours hours remaining this week.', {'@hours': hoursRemainingThisWeek});
      $(".form-item-hours .hours__message").text(currentHoursTextMessage);
    }
  };
}(jQuery));