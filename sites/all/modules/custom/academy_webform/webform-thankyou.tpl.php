<?php

/**
 * @file
 * Custom thanks page for a submitted webform.
 *
 * Available variables:
 * - $username: user that submitted the form.
 */
?>

<div class="webform__thankyou">

  <h2 class="webform__thankyou--title">
    <?php print t('Dear @username,', array('@username' => $username)); ?>
  </h2>

  <div class="webform--thankyou--content">
    <p><?php print t('Thank you for your question. We will try to answer as soon as possible.'); ?></p>
  </div>

</div>
