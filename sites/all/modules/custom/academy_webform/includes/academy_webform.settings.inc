<?php
/**
 * @file
 * Code for the Webform settings page.
 *
 * A user can fill in the NID of the webform he/she want's to redirect to
 * a custom page.
 */

/**
 * Webform admin settings page callback, referenced in hook_menu().
 */
function academy_webform_settings($form, &$form_state) {

  $form = array();

  $form['academy_webform_nid'] = array(
    '#type' => 'textfield',
    '#title' => t('Webform NID'),
    '#description' => t('Enter the NID of the webform that should be 
      redirected to the custom thank-you page.'),
    '#default_value' => variable_get('academy_webform_nid'),
  );

  return system_settings_form($form);

}