<?php
/**
 * @file
 * academy_klant.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function academy_klant_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create klant content'.
  $permissions['create klant content'] = array(
    'name' => 'create klant content',
    'roles' => array(
      'Beheerder' => 'Beheerder',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any klant content'.
  $permissions['delete any klant content'] = array(
    'name' => 'delete any klant content',
    'roles' => array(
      'Beheerder' => 'Beheerder',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own klant content'.
  $permissions['delete own klant content'] = array(
    'name' => 'delete own klant content',
    'roles' => array(
      'Beheerder' => 'Beheerder',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any klant content'.
  $permissions['edit any klant content'] = array(
    'name' => 'edit any klant content',
    'roles' => array(
      'Beheerder' => 'Beheerder',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own klant content'.
  $permissions['edit own klant content'] = array(
    'name' => 'edit own klant content',
    'roles' => array(
      'Beheerder' => 'Beheerder',
    ),
    'module' => 'node',
  );

  return $permissions;
}
